package com.flight.service;

import com.flight.factory.PlaneFactory;
import com.flight.model.planes.Plane;
import com.flight.model.planes.RequiredParameters;
import com.flight.model.planes.WeightAndArm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LimitServiceIsWithinUpperBoundTest {

    LimitService limitService = new LimitService();

    @Test
    void test1() {
        RequiredParameters input = createInput(2600);
        Plane plane = createPlane(input, 88);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertFalse(isWithinUpperBound);
    }

    @Test
    void test2() {
        RequiredParameters input = createInput(2600);
        Plane plane = createPlane(input, 92);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertFalse(isWithinUpperBound);
    }

    @Test
    void test3() {
        RequiredParameters input = createInput(2400);
        Plane plane = createPlane(input, 90);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertTrue(isWithinUpperBound);
    }

    @Test
    void test4() {
        RequiredParameters input = createInput(2000);
        Plane plane = createPlane(input, 88);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertTrue(isWithinUpperBound);
    }

    @Test
    void test5() {
        RequiredParameters input = createInput(1600);
        Plane plane = createPlane(input, 86);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertFalse(isWithinUpperBound);
    }

    @Test
    void test6() {
        RequiredParameters input = createInput(1400);
        Plane plane = createPlane(input, 88);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertTrue(isWithinUpperBound);
    }

    @Test
    void test7() {
        RequiredParameters input = createInput(1400);
        Plane plane = createPlane(input, 92);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertTrue(isWithinUpperBound);
    }

    @Test
    void test8() {
        RequiredParameters input = createInput(1400);
        Plane plane = createPlane(input, 94);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertFalse(isWithinUpperBound);
    }

    @Test
    void test9() {
        RequiredParameters input = createInput(1600);
        Plane plane = createPlane(input, 94);
        boolean isWithinUpperBound = limitService.isWithinUpperBound(input, plane);
        assertFalse(isWithinUpperBound);
    }

    private RequiredParameters createInput(int weight) {
        return RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(weight - 1520)
                .build();
    }

    private Plane createPlane(RequiredParameters input, final double fakeCenterOfGravity) {
        Plane plane = new Plane() {
            @Override
            public double calculateCenterOfGravity(RequiredParameters input) {
                return fakeCenterOfGravity;
            }
        };
        update(input, plane);
        return plane;
    }

    private void update(RequiredParameters input, Plane plane) {
        Plane warrior2 = new PlaneFactory().createWarrior2();
        plane.setName(warrior2.getName());
        plane.setMinCenterOfGravity(warrior2.getMinCenterOfGravity());
        plane.setMinUpperBoundCenterOfGravity(warrior2.getMinUpperBoundCenterOfGravity());
        plane.setMaxWeightOfLowerBound(warrior2.getMaxWeightOfLowerBound());
        plane.setMaxCenterOfGravity(warrior2.getMaxCenterOfGravity());
        plane.setMaxGrossWeight(warrior2.getMaxGrossWeight());
        plane.setBasicEmptyWeight(WeightAndArm.builder().weight(1520).build());
        plane.setPilotAndFrontPassenger(WeightAndArm.builder().weight(input.getPilotAndFrontPassengerWeight()).build());
        plane.setPassengerRearSeat(WeightAndArm.builder().weight(input.getPassengerRearSeatWeight()).build());
        plane.setFuel(WeightAndArm.builder().weight(input.getFuelWeight()).build());
        plane.setBaggage(WeightAndArm.builder().weight(input.getBaggageWeight()).build());
    }
}
