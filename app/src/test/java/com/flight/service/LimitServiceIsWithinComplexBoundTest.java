package com.flight.service;

import com.flight.model.planes.Plane;
import com.flight.model.planes.RequiredParameters;
import com.flight.model.planes.WeightAndArm;
import com.flight.repository.PlaneRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;

class LimitServiceIsWithinComplexBoundTest {

    LimitService limitService = new LimitService();
    PlaneRepository repository = new PlaneRepository();

    @Test
    void test10() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(1800 - 1520)
                .build();
        Plane plane = new Plane() {
            @Override
            public double calculateCenterOfGravity(RequiredParameters input) {
                return 96;
            }
        };
        update(input, plane);
        boolean onTheRightSideOfTheGraph = limitService.isWithinComplexBound(input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test11() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(1830 - 1520)
                .build();
        Plane plane = new Plane() {
            @Override
            public double calculateCenterOfGravity(RequiredParameters input) {
                return 96;
            }
        };
        update(input, plane);
        boolean onTheRightSideOfTheGraph = limitService.isWithinComplexBound(input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test12() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2600 - 1520)
                .build();
        Plane plane = new Plane() {
            @Override
            public double calculateCenterOfGravity(RequiredParameters input) {
                return 96;
            }
        };
        update(input, plane);
        boolean onTheRightSideOfTheGraph = limitService.isWithinComplexBound(input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    private void update(RequiredParameters input, Plane plane) {
        plane.setBasicEmptyWeight(WeightAndArm.builder().weight(1520).build());
        plane.setPilotAndFrontPassenger(WeightAndArm.builder().weight(input.getPilotAndFrontPassengerWeight()).build());
        plane.setPassengerRearSeat(WeightAndArm.builder().weight(input.getPassengerRearSeatWeight()).build());
        plane.setFuel(WeightAndArm.builder().weight(input.getFuelWeight()).build());
        plane.setBaggage(WeightAndArm.builder().weight(input.getBaggageWeight()).build());
    }
}
