package com.flight.service;

import com.flight.model.planes.RequiredParameters;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

class LimitServiceIsBalancedTest {

    LimitService limitService = new LimitService();

    @Test
    void test1() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1200);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test2() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1300);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test3() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1400);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test4() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1500);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test5() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1600);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test6() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1700);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test7() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1800);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test8() {
        RequiredParameters input = createInputWithFrontPassengerWeight(1900);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test9() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2000);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isTrue();
    }

    @Test
    void test10() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2100);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    void test11() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2100);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    void test12() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2200);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    void test13() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2300);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertFalse(isWithinBounds);
    }

    @Test
    void test14() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2400);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    void test15() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2500);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    void test16() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2600);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    void test17() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2700);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    void test18() {
        RequiredParameters input = createInputWithFrontPassengerWeight(2800);
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is"
                        + input.getPilotAndFrontPassengerWeight()
                        + "the plane is balanced.")
                .isFalse();
    }

    @Test
    @DisplayName("Weight is 2450 and CG 89")
    void testRealistic1() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .baggageWeight(100)
                .pilotAndFrontPassengerWeight(500)
                .fuelWeight(30 * 6)
                .passengerRearSeatWeight(150)
                .build();
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is 2450"
                        + " and Center of Gravity is 89.45 "
                        + "the plane is not balanced.")
                .isFalse();
    }

    @Test
    void testRealistic2() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .baggageWeight(100)
                .pilotAndFrontPassengerWeight(500)
                .fuelWeight(30 * 6)
                .passengerRearSeatWeight(100)
                .build();
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is 2400"
                        + " and Center of Gravity is 88.85 "
                        + "the plane is not balanced.")
                .isTrue();
    }

    @Test
    void testIsExactlyMaxGrossWeight() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .baggageWeight(90)
                .pilotAndFrontPassengerWeight(500)
                .fuelWeight(30 * 6)
                .passengerRearSeatWeight(150)
                .build();
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is 2440"
                        + " and Center of Gravity is 89.23 "
                        + "the plane is not balanced.")
                .isTrue();
    }

    @Test
    void testRealistic3() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .baggageWeight(101)
                .pilotAndFrontPassengerWeight(500)
                .fuelWeight(30 * 6)
                .passengerRearSeatWeight(100)
                .build();
        boolean isWithinBounds = limitService.isBalanced(input);
        assertThat(isWithinBounds)
                .describedAs("When weight is 2401"
                        + " and Center of Gravity is 88.85 "
                        + "the plane is not balanced.")
                .isTrue();
    }


    private RequiredParameters createInputWithFrontPassengerWeight(int weight) {
        return RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(weight - 1520)
                .build();
    }
}
