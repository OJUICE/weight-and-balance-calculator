package com.flight.service;

import com.flight.model.planes.Plane;
import com.flight.model.planes.RequiredParameters;
import com.flight.repository.PlaneRepository;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LimitServiceIsOnTheRightSideOfTheGraphTest {

    LimitService limitService = new LimitService();
    PlaneRepository repository = new PlaneRepository();

    @Test
    void test1() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2400 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(82, input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test2() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2400 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(84, input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test3() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2400 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(86, input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test4() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2200 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(82, input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test5() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2200 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(84, input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test6() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2200 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(86, input, plane);
        assertTrue(onTheRightSideOfTheGraph);
    }

    @Test
    void test7() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2000 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(82, input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test8() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2000 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(84, input, plane);
        assertTrue(onTheRightSideOfTheGraph);
    }

    @Test
    void test9() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2000 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(86, input, plane);
        assertTrue(onTheRightSideOfTheGraph);
    }

    @Test
    void test10() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(1800 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(82, input, plane);
        assertTrue(onTheRightSideOfTheGraph);
    }

    @Test
    void test11() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(1830 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(82, input, plane);
        assertFalse(onTheRightSideOfTheGraph);
    }

    @Test
    void test12() {
        RequiredParameters input = RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(2600 - 1520)
                .build();
        Plane plane = repository.findByName(input);
        boolean onTheRightSideOfTheGraph = limitService.isOnTheRightSideOfTheGraph(90, input, plane);
        assertTrue(onTheRightSideOfTheGraph);
    }
}
