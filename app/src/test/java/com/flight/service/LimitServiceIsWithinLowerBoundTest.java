package com.flight.service;

import com.flight.factory.PlaneFactory;
import com.flight.model.planes.Plane;
import com.flight.model.planes.RequiredParameters;
import com.flight.model.planes.WeightAndArm;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

class LimitServiceIsWithinLowerBoundTest {

    LimitService limitService = new LimitService();

    @Test
    void test1() {
        RequiredParameters input = createInput(2000);
        Plane plane = createPlane(input, 82);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertFalse(isWithinLowerBound);
    }

    @Test
    void test2() {
        RequiredParameters input = createInput(2000);
        Plane plane = createPlane(input, 84);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertFalse(isWithinLowerBound);
    }

    @Test
    void test3() {
        RequiredParameters input = createInput(2000);
        Plane plane = createPlane(input, 88);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertFalse(isWithinLowerBound);
    }

    @Test
    void test4() {
        RequiredParameters input = createInput(2000);
        Plane plane = createPlane(input, 88);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertFalse(isWithinLowerBound);
    }

    @Test
    void test5() {
        RequiredParameters input = createInput(1800);
        Plane plane = createPlane(input, 82);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertFalse(isWithinLowerBound);
    }

    @Test
    void test6() {
        RequiredParameters input = createInput(1800);
        Plane plane = createPlane(input, 86);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertTrue(isWithinLowerBound);
    }

    @Test
    void test7() {
        RequiredParameters input = createInput(1800);
        Plane plane = createPlane(input, 88);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertFalse(isWithinLowerBound);
    }

    @Test
    void test8() {
        RequiredParameters input = createInput(1200);
        Plane plane = createPlane(input, 82);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertFalse(isWithinLowerBound);
    }

    @Test
    void test9() {
        RequiredParameters input = createInput(1200);
        Plane plane = createPlane(input, 84);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertTrue(isWithinLowerBound);
    }

    @Test
    void test10() {
        RequiredParameters input = createInput(1100);
        Plane fakePlane = createPlane(input, 86);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, fakePlane);
        assertTrue(isWithinLowerBound);
    }

    @Test
    void test11() {
        RequiredParameters input = createInput(1200);
        Plane plane = createPlane(input, 86.5);
        boolean isWithinLowerBound = limitService.isWithinLowerBound(input, plane);
        assertTrue(isWithinLowerBound);
    }

    private RequiredParameters createInput(int weight) {
        return RequiredParameters.builder()
                .plane("Warrior 2")
                .pilotAndFrontPassengerWeight(weight - 1520)
                .build();
    }

    private Plane createPlane(RequiredParameters input, final double fakeCenterOfGravity) {
        Plane plane = new Plane() {
            @Override
            public double calculateCenterOfGravity(RequiredParameters input) {
                return fakeCenterOfGravity;
            }
        };
        update(input, plane);
        return plane;
    }

    private void update(RequiredParameters input, Plane plane) {
        Plane warrior2 = new PlaneFactory().createWarrior2();
        plane.setName(warrior2.getName());
        plane.setMinCenterOfGravity(warrior2.getMinCenterOfGravity());
        plane.setMinUpperBoundCenterOfGravity(warrior2.getMinUpperBoundCenterOfGravity());
        plane.setMaxWeightOfLowerBound(warrior2.getMaxWeightOfLowerBound());
        plane.setMaxCenterOfGravity(warrior2.getMaxCenterOfGravity());
        plane.setMaxGrossWeight(warrior2.getMaxGrossWeight());
        plane.setBasicEmptyWeight(WeightAndArm.builder().weight(1520).build());
        plane.setPilotAndFrontPassenger(WeightAndArm.builder().weight(input.getPilotAndFrontPassengerWeight()).build());
        plane.setPassengerRearSeat(WeightAndArm.builder().weight(input.getPassengerRearSeatWeight()).build());
        plane.setFuel(WeightAndArm.builder().weight(input.getFuelWeight()).build());
        plane.setBaggage(WeightAndArm.builder().weight(input.getBaggageWeight()).build());
    }
}
