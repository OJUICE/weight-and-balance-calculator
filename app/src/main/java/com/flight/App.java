/*
 * This Java source file was generated by the Gradle 'init' task.
 */
package com.flight;

import com.flight.gui.MainFrame;
import com.flight.model.planes.RequiredParameters;
import com.flight.service.LimitService;

import javax.swing.*;
import java.util.Scanner;

public class App {

    LimitService limitService = new LimitService();
    MainFrame mainFrame = new MainFrame();

    public String getGreeting() {
        return "Welcome to Weight and Balance Calculator of Single Engine Planes";
    }

    public static void main(String[] args) {
        setDefaultLookAndFeel();
        App app = new App();
        System.out.println(app.getGreeting());
        Scanner s = new Scanner(System.in);

//        System.out.println("\nPlease enter Plane name: ");
//        String planeName = s.nextLine();
        String planeName = "Warrior 2";

//        System.out.println("\nPlease enter Weight of Pilot and Front Passenger:");
//        int pilotAndFrontPassengerWeight = s.nextInt();
        int pilotAndFrontPassengerWeight = 200;

//        System.out.println("\nPlease enter Weight of Rear Passenger(s): ");
//        int passengerRearSeatWeight = s.nextInt();
        int passengerRearSeatWeight = 100;

//        System.out.println("\nPlease enter Weight of Fuel: ");
//        int fuelWeight = s.nextInt();
        int fuelWeight = 200;

//        System.out.println("\nPlease enter Weight of Baggage: ");
//        int baggageWeight = s.nextInt();
        int baggageWeight = 0;


        RequiredParameters input = RequiredParameters.builder()
                .pilotAndFrontPassengerWeight(pilotAndFrontPassengerWeight)
                .passengerRearSeatWeight(passengerRearSeatWeight)
                .fuelWeight(fuelWeight)
                .baggageWeight(baggageWeight)
                .plane(planeName)
                .build();

        boolean balanced = app.isBalanced(input);
        System.out.println("Center of Gravity is in limits: " + balanced);
        app.startGui();
    }

    private static void setDefaultLookAndFeel() {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (UnsupportedLookAndFeelException
                | IllegalAccessException
                | InstantiationException
                | ClassNotFoundException e) {
            System.err.println("Unsupported Look and Feel");
        }
    }

    public boolean isBalanced(RequiredParameters input) {
        return limitService.isBalanced(input);
    }

    public JFrame startGui() {
        return mainFrame.createMainWindow();
    }
}
