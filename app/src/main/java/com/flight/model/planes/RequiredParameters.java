package com.flight.model.planes;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class RequiredParameters {
    //private double basicEmptyWeight;
    private double pilotAndFrontPassengerWeight;
    private double passengerRearSeatWeight;
    private double fuelWeight;
    private double baggageWeight;
    private String plane;
}
