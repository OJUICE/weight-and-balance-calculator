package com.flight.model.planes;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class WeightAndArm {

    private String property;
    private double weight;
    private double arm;
    private double emptyMoment;

    public double getMoment() {
        return weight * arm;
    }
}
