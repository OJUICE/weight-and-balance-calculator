package com.flight.model.planes;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class Plane {

    String model;
    String name;
    WeightAndArm basicEmptyWeight;
    WeightAndArm pilotAndFrontPassenger;
    WeightAndArm passengerRearSeat;
    WeightAndArm fuel;
    WeightAndArm baggage;
    int minCenterOfGravity;
    int minUpperBoundCenterOfGravity;

    /**
     * the same as minWeightOfUpperBound
     */
    int maxWeightOfLowerBound;
    int maxCenterOfGravity;
    int maxGrossWeight;
    double complexFunctionFxFactor;
    double complexFunctionY;

    public double calculateTotalWeight(RequiredParameters userInput) {
        update(userInput);
        return basicEmptyWeight.getWeight()
                + userInput.getPilotAndFrontPassengerWeight()
                + userInput.getPassengerRearSeatWeight()
                + userInput.getFuelWeight()
                + userInput.getBaggageWeight();
    }

    private double calculateTotalMoment(RequiredParameters userInput) {
        update(userInput);
        return basicEmptyWeight.getEmptyMoment()
                + pilotAndFrontPassenger.getMoment()
                + passengerRearSeat.getMoment()
                + fuel.getMoment()
                + baggage.getMoment();
    }

    /**
     * this is only a declaration of the updated values for the weight.
     *
     * @param input User input with 4 required parameters
     */
    private void update(RequiredParameters input) {
        this.pilotAndFrontPassenger.setWeight(input.getPilotAndFrontPassengerWeight());
        this.passengerRearSeat.setWeight(input.getPassengerRearSeatWeight());
        this.fuel.setWeight(input.getFuelWeight());
        this.baggage.setWeight(input.getBaggageWeight());
    }

    public double calculateCenterOfGravity(RequiredParameters input) {
        return (calculateTotalMoment(input) / calculateTotalWeight(input));
    }
}
