package com.flight.service;

import com.flight.model.planes.Plane;
import com.flight.model.planes.RequiredParameters;
import com.flight.repository.PlaneRepository;

public class LimitService {

    PlaneRepository repository = new PlaneRepository();

    public boolean isBalanced(RequiredParameters input) {
        Plane plane = repository.findByName(input);
        System.out.print("Calculated Center of Gravity: ");
        System.out.println(plane.calculateCenterOfGravity(input));
        System.out.print("Calculated Total Weight: ");
        System.out.println(plane.calculateTotalWeight(input));
        if (isWithinUpperBound(input, plane)
                || isWithinLowerBound(input, plane)
                || isWithinComplexBound(input, plane)) {
            return true;
        }
        return false;
    }

    /**
     * is in range between (min upper bound <= x <= max)
     *
     * @param input
     * @param plane
     * @return
     */
    boolean isWithinUpperBound(RequiredParameters input, Plane plane) {
        double x = plane.calculateCenterOfGravity(input);
        double y = plane.calculateTotalWeight(input);
        return (plane.getMinUpperBoundCenterOfGravity() <= x && x <= plane.getMaxCenterOfGravity())
                && (y <= plane.getMaxGrossWeight());
    }

    /**
     * is in range between (min <= x < min upper bound ) && x < max weight lower bound
     *
     * @param input
     * @param plane
     * @return
     */
    boolean isWithinLowerBound(RequiredParameters input, Plane plane) {
        double x = plane.calculateCenterOfGravity(input);
        double y = plane.calculateTotalWeight(input);
        return plane.getMinCenterOfGravity() <= x && x < plane.getMinUpperBoundCenterOfGravity()
                && plane.calculateTotalWeight(input) < plane.getMaxWeightOfLowerBound()
                && (y <= plane.getMaxGrossWeight());
    }

    /**
     * x is in range between (min <= x < min upper bound ) && x >= max weight lower bound
     * y (total weight) is less than expectedY (y = 81.6a - 5149)
     *
     * @param input
     * @param plane
     * @return
     */
    boolean isWithinComplexBound(RequiredParameters input, Plane plane) {
        double x = plane.calculateCenterOfGravity(input);
        double y = plane.calculateTotalWeight(input);
        return isOnTheRightSideOfTheGraph(x, input, plane)
                && plane.calculateTotalWeight(input) >= plane.getMaxWeightOfLowerBound()
                && (y <= plane.getMaxGrossWeight());
    }

    boolean isOnTheRightSideOfTheGraph(double x, RequiredParameters input, Plane plane) {
        double expectedY = complexFunction(x, plane);
        if (plane.calculateTotalWeight(input) <= expectedY) {
            return true;
        } else return false;
    }
    // private double complexFunction(double x) {
    //     return 122.5 * x - 8217.5;
    // }

    private double complexFunction(double x, Plane plane) {
        return plane.getComplexFunctionFxFactor() * x - plane.getComplexFunctionY();
    }

    public boolean isTotalWeightInRange(RequiredParameters input) {
        Plane plane = repository.findByName(input);
        double totalWeight = plane.calculateTotalWeight(input);
        double maxGrossWeight = plane.getMaxGrossWeight();
        if (plane.getBasicEmptyWeight().getWeight() <= totalWeight && totalWeight <= maxGrossWeight) {
            return true;
        } else {
            return false;
        }
    }
}
