package com.flight.repository;

import com.flight.factory.PlaneFactory;
import com.flight.model.planes.Plane;
import com.flight.model.planes.RequiredParameters;
import lombok.Data;

import java.util.List;
import java.util.stream.Collectors;

@Data
public class PlaneRepository {

    PlaneFactory planeFactory = new PlaneFactory();

    List<Plane> planes = List.of(
            planeFactory.createWarrior2(),
            planeFactory.createCessna172());


    /**
     * select top(1) * from planes p
     * where p.name = input.getPlane()
     *
     * @param input
     * @return
     */
    public Plane findByName(RequiredParameters input) {
        return planes.stream()
                .filter(p -> p.getName().equals(input.getPlane()))
                .collect(Collectors.toList())
                .get(0);
    }

    public Plane findByName(String input) {
        return planes.stream()
                .filter(p -> p.getName().equals(input))
                .collect(Collectors.toList())
                .get(0);
    }
}
