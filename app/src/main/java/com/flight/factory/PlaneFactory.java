package com.flight.factory;

import com.flight.model.planes.Plane;
import com.flight.model.planes.WeightAndArm;

public class PlaneFactory {

    public Plane createWarrior2() {
        return Plane.builder()
                .model("PA-28.161")
                .name("Warrior 2")
                .basicEmptyWeight(WeightAndArm.builder()
                        .property("Basic Empty Weight")
                        .weight(1520.00)
                        .emptyMoment(129808.00)
                        .build())
                .pilotAndFrontPassenger(WeightAndArm.builder()
                        .property("Pilot and Front Passenger")
                        .arm(80.5)
                        .build())
                .passengerRearSeat(WeightAndArm.builder()
                        .property("Passenger Rear Seat")
                        .arm(118.1)
                        .build())
                .fuel(WeightAndArm.builder()
                        .property("Fuel")
                        .arm(95.0)
                        .build())
                .baggage(WeightAndArm.builder()
                        .property("Baggage")
                        .arm(142.8)
                        .build())
                .minCenterOfGravity(83)
                .minUpperBoundCenterOfGravity(87)
                .maxWeightOfLowerBound(1950)
                .maxCenterOfGravity(93)
                .maxGrossWeight(2440)
                .complexFunctionFxFactor(122.5)
                .complexFunctionY(8217.5)
                .build();
    }

    public Plane createCessna172() {
        return Plane.builder()
                .model("Cessna 172N")
                .name("Cessna")
                .basicEmptyWeight(WeightAndArm.builder()
                        .property("Basic Empty Weight")
                        .weight(1550.00)
                        .emptyMoment(61.54)
                        .build())
                .pilotAndFrontPassenger(WeightAndArm.builder()
                        .property("Pilot and Front Passenger")
                        .arm(37)
                        .build())
                .passengerRearSeat(WeightAndArm.builder()
                        .property("Passenger Rear Seat")
                        .arm(73)
                        .build())
                .fuel(WeightAndArm.builder()
                        .property("Fuel")
                        .arm(48)
                        .build())
                .baggage(WeightAndArm.builder()
                        .property("Baggage")
                        .arm(108)
                        .build())
                .minCenterOfGravity(62)
                .minUpperBoundCenterOfGravity(67)
                .maxWeightOfLowerBound(1500)
                .maxCenterOfGravity(73)
                .maxGrossWeight(2100)
                .complexFunctionFxFactor(122.5)
                .complexFunctionY(8217.5)
                .build();
    }
}
