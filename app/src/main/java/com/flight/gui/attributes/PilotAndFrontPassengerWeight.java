package com.flight.gui.attributes;

import lombok.Data;

import javax.swing.*;
import java.awt.*;

import static com.flight.gui.attributes.Constants.WIDTH;

@Data
public class PilotAndFrontPassengerWeight {

    JTextField jTextField;

    public JPanel create() {
        JPanel panel = new JPanel();
        panel.setSize(WIDTH, 50);
        panel.setMaximumSize(new Dimension(WIDTH, 200));
        panel.setLocation(10, 60 + 50 + 50);
        panel.setBackground(Color.RED);
        JLabel model = createPilotWeightLabel();
        jTextField = createJtextField();
        panel.add(model);
        panel.add(jTextField);
        panel.setVisible(true);
        return panel;
    }

    private JLabel createPilotWeightLabel() {
        return new JLabel("Pilot and Front Passenger Weight:");
    }

    private JTextField createJtextField() {
        JTextField jTextField = new JTextField();
        jTextField.setColumns(10);
        return jTextField;
    }

    public double getValue() {
        return Double.valueOf(jTextField.getText());
    }

}
