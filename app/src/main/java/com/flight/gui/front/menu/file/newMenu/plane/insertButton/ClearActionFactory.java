package com.flight.gui.front.menu.file.newMenu.plane.insertButton;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ClearActionFactory {

    public ActionListener create(TextFieldsDto dto) {
        ActionListener ac = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dto.getList().forEach(textField -> textField.setText(""));
            }
        };
        return ac;
    }
}
