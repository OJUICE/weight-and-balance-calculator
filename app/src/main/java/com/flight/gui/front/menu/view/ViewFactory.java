package com.flight.gui.front.menu.view;

import javax.swing.*;

public class ViewFactory {

    public JMenu create() {
        JMenu menu = new JMenu("View");
        JMenuItem item1 = new JMenuItem("Zoom");
        JMenuItem item2 = new JMenuItem("Inspect");
        menu.add(item1);
        menu.add(item2);
        JMenu subMenu = createSubMenu();
        menu.add(subMenu);
        return menu;
    }


    private JMenu createSubMenu() {
        SubmenuFactory factory = new SubmenuFactory();
        JMenu jMenu = factory.create();
        return jMenu;
    }

}
