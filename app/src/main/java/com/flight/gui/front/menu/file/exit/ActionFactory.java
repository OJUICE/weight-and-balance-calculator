package com.flight.gui.front.menu.file.exit;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionFactory {

    public ActionListener create() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int input = JOptionPane.showConfirmDialog(null, "Are you sure you want to Exit?");
                if (input == 0) {
                    System.exit(0);
                }
            }
        };
        return action;
    }
}
