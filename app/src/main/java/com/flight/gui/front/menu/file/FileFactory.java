package com.flight.gui.front.menu.file;

import com.flight.gui.front.menu.file.exit.ExitFactory;

import javax.swing.*;

public class FileFactory {

    public JMenu create() {
        JMenu menu = new JMenu("File");
        JMenu newMenu = createSubmenu();
        JMenuItem item2 = createSettings();
        JMenuItem item3 = createExit();
        menu.add(newMenu);
        menu.add(item2);
        menu.add(item3);
        return menu;
    }

    private JMenu createSubmenu() {
        SubmenuFactory factory = new SubmenuFactory();
        JMenu jMenu = factory.create();
        return jMenu;
    }

    private JMenuItem createSettings() {
        JMenuItem item = new JMenuItem("Settings");
        return item;
    }

    private JMenuItem createExit() {
        ExitFactory factory = new ExitFactory();
        JMenuItem item = factory.create();
        return item;
    }
}
