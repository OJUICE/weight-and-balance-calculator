package com.flight.gui.front.menu.view;

import javax.swing.*;

public class SubSubmenuFactory {

    public JMenu create() {
        JMenu subMenu = new JMenu("More");
        JMenuItem subMenu1Item = createSubSubmenu1();
        JMenuItem subMenu2Item = createSubSubmenu2();
        subMenu.add(subMenu1Item);
        subMenu.add(subMenu2Item);
        return subMenu;
    }

    private JMenuItem createSubSubmenu1() {
        JMenuItem jMenuItem = new JMenuItem("SubMenuItem1");
        return jMenuItem;
    }

    private JMenuItem createSubSubmenu2() {
        JMenuItem jMenuItem = new JMenuItem("SubMenuItem2");
        return jMenuItem;
    }
}
