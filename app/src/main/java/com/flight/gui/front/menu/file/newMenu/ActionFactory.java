package com.flight.gui.front.menu.file.newMenu;

import com.flight.gui.front.menu.file.newMenu.plane.*;
import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ActionFactory {

    public ActionListener create() {
        ActionListener action = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                createFrame();
            }
        };
        return action;
    }

    private JFrame createFrame() {
        JFrame j = new JFrame("New Aeroplane");
        j.setSize(400, 500);
        j.setVisible(true);
        j.setLayout(null);
        j.setLocation(500, 80);
        j.setBackground(Color.CYAN);
        j.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        TextFieldsDto dto = new TextFieldsDto();
        j.add(createModelPanel(dto));
        j.add(createNamePanel(dto));
        j.add(createBasicEmptyWeightPanel(dto));
        j.add(createPilotAndFrontPassengerPanel(dto));
        j.add(createPassengerRearSeatPanel(dto));
        j.add(createFuelPanel(dto));
        j.add(createBaggagePanel(dto));
        j.add(createMinCgPanel(dto));
        j.add(createMaxCgPanel(dto));
        j.add(createMaxGrossWeightPanel(dto));
        j.add(createButtonPanel(dto));
        return j;
    }


    private JPanel createModelPanel(TextFieldsDto dto) {
        ModelPanel mp = new ModelPanel();
        JPanel panel = mp.create(dto);
        return panel;
    }

    private JPanel createNamePanel(TextFieldsDto dto) {
        NamePanel np = new NamePanel();
        JPanel jPanel = np.create(dto);
        return jPanel;
    }

    private JPanel createBasicEmptyWeightPanel(TextFieldsDto dto) {
        BasicEmptyWeightPanel bp = new BasicEmptyWeightPanel();
        JPanel jPanel = bp.create(dto);
        return jPanel;
    }

    private JPanel createPilotAndFrontPassengerPanel(TextFieldsDto dto) {
        PilotAndFrontPassengerPanel pp = new PilotAndFrontPassengerPanel();
        JPanel jPanel = pp.create(dto);
        return jPanel;
    }

    private JPanel createPassengerRearSeatPanel(TextFieldsDto dto) {
        PassengerRearSeatPanel prp = new PassengerRearSeatPanel();
        JPanel jPanel = prp.create(dto);
        return jPanel;
    }

    private JPanel createFuelPanel(TextFieldsDto dto) {
        FuelPanel fp = new FuelPanel();
        JPanel jPanel = fp.create(dto);
        return jPanel;
    }

    private JPanel createBaggagePanel(TextFieldsDto dto) {
        BaggagePanel bp = new BaggagePanel();
        JPanel jPanel = bp.create(dto);
        return jPanel;
    }

    private JPanel createMinCgPanel(TextFieldsDto dto) {
        MinCgPanel mp = new MinCgPanel();
        JPanel jPanel = mp.create(dto);
        return jPanel;
    }

    private JPanel createMaxCgPanel(TextFieldsDto dto) {
        MaxCgPanel mp = new MaxCgPanel();
        JPanel jPanel = mp.create(dto);
        return jPanel;
    }

    private JPanel createMaxGrossWeightPanel(TextFieldsDto dto) {
        MaxGrossWeightPanel mp = new MaxGrossWeightPanel();
        JPanel jPanel = mp.create(dto);
        return jPanel;
    }

    private JPanel createButtonPanel(TextFieldsDto dto) {
        InsertButtonPanel factory = new InsertButtonPanel();
        JPanel jPanel = factory.create(dto);
        return jPanel;

    }


}
