package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;

public class BaggagePanel {

    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createLabel());
        JTextField baggage = createTextfield();
        dto.setBaggage(baggage);
        dto.getList().add(baggage);
        panel.add(baggage);
        panel.setLocation(10, 10 + 200);
        panel.setSize(365, 30);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JLabel createLabel() {
        JLabel label = new JLabel("Baggage Arm: ");
        return label;
    }

    private JTextField createTextfield() {
        JTextField text = new JTextField();
        text.setColumns(20);
        return text;
    }
}
