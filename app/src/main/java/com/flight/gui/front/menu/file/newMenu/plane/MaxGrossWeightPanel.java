package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;

public class MaxGrossWeightPanel {

    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createLabel());
        JTextField maxgw = createTextfield();
        dto.setMaxGrossWeight(maxgw);
        dto.getList().add(maxgw);
        panel.add(maxgw);
        panel.setLocation(10, 10 + 290);
        panel.setSize(365, 30);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JLabel createLabel() {
        JLabel label = new JLabel("Maximum Gross Weight: ");
        return label;
    }

    private JTextField createTextfield() {
        JTextField text = new JTextField();
        text.setColumns(20);
        return text;
    }
}
