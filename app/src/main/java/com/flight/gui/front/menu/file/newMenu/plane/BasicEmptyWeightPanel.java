package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;

public class BasicEmptyWeightPanel {

    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createLabel());
        JTextField weight = createTextfield();
        dto.setBasicEmptyWeight(weight);
        dto.getList().add(weight);
        panel.add(weight);
        panel.add(createLabel1());
        JTextField moment = createTextfield();
        dto.setMoment(moment);
        dto.getList().add(moment);
        panel.add(moment);
        panel.setLocation(10, 10 + 30 + 30);
        panel.setSize(365, 30);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JLabel createLabel() {
        JLabel label = new JLabel("Weight: ");
        return label;
    }

    private JLabel createLabel1() {
        JLabel label = new JLabel("Moment: ");
        return label;
    }

    private JTextField createTextfield() {
        JTextField text = new JTextField();
        text.setColumns(10);
        return text;
    }


}
