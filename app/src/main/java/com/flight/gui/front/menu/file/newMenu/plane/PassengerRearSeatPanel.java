package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;

public class PassengerRearSeatPanel {

    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createLabel());
        JTextField prs = createTextfield();
        dto.setPassengerRearSeat(prs);
        dto.getList().add(prs);
        panel.add(prs);
        panel.setLocation(10, 10 + 140);
        panel.setSize(365, 50);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JLabel createLabel() {
        JLabel label = new JLabel("Passenger Rear Seat Arm: ");
        return label;
    }

    private JTextField createTextfield() {
        JTextField text = new JTextField();
        text.setColumns(20);
        return text;
    }
}
