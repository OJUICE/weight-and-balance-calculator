package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;

public class PilotAndFrontPassengerPanel {
    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createLabel());
        JTextField pfp = createTextfield();
        dto.setPilotAndFrontPassenger(pfp);
        dto.getList().add(pfp);
        panel.add(pfp);
        panel.setLocation(10, 10 + 90);
        panel.setSize(365, 50);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JLabel createLabel() {
        JLabel label = new JLabel("Pilot and Front Passenger Arm: ");
        return label;
    }

    private JTextField createTextfield() {
        JTextField text = new JTextField();
        text.setColumns(20);
        return text;
    }
}
