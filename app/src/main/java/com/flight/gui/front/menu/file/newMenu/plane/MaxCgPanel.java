package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;

public class MaxCgPanel {

    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createLabel());
        JTextField maxcg = createTextfield();
        dto.setMaxCg(maxcg);
        dto.getList().add(maxcg);
        panel.add(maxcg);
        panel.setLocation(10, 10 + 260);
        panel.setSize(365, 30);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JLabel createLabel() {
        JLabel label = new JLabel("Max CG: ");
        return label;
    }

    private JTextField createTextfield() {
        JTextField text = new JTextField();
        text.setColumns(20);
        return text;
    }
}
