package com.flight.gui.front.menu.edit;

import javax.swing.*;

public class EditFactory {

    public JMenu create() {
        JMenu menu = new JMenu("Edit");
        JMenuItem item1 = createSave();
        JMenuItem item2 = createCut();
        JMenuItem item3 = createCopy();
        JMenuItem item4 = createPaste();
        JMenuItem item5 = createClear();
        menu.add(item1);
        menu.add(item2);
        menu.add(item3);
        menu.add(item4);
        menu.add(item5);
        return menu;
    }

    private JMenuItem createSave() {
        JMenuItem item = new JMenuItem("Save");
        return item;
    }

    private JMenuItem createCut() {
        JMenuItem item = new JMenuItem("Cut");
        return item;
    }

    private JMenuItem createCopy() {
        JMenuItem item = new JMenuItem("Copy");
        return item;
    }

    private JMenuItem createPaste() {
        JMenuItem item = new JMenuItem("Paste");
        return item;
    }

    private JMenuItem createClear() {
        JMenuItem item = new JMenuItem("Clear");
        return item;
    }

}
