package com.flight.gui.front.menu.file;

import com.flight.gui.front.menu.file.newMenu.PlaneItemFactory;

import javax.swing.*;

public class SubmenuFactory {

    public JMenu create() {
        JMenu subMenu = new JMenu("New");
        JMenuItem planeItem = createPlane();
        subMenu.add(planeItem);
        return subMenu;
    }

    private JMenuItem createPlane() {
        PlaneItemFactory factory = new PlaneItemFactory();
        JMenuItem jMenuItem = factory.create();
        return jMenuItem;
    }
}
