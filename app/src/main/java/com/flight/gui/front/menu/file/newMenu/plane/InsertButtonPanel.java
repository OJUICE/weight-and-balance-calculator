package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.ClearActionFactory;
import com.flight.gui.front.menu.file.newMenu.plane.insertButton.SaveActionFactory;
import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class InsertButtonPanel {

    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createSaveButton(dto));
        panel.add(createClearButton(dto));
        panel.setLocation(10, 10 + 340);
        panel.setSize(365, 50);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JButton createClearButton(TextFieldsDto dto) {
        JButton jButton = new JButton("Clear");
        jButton.setName("Clear");
        jButton.setSize(createDimension());
        jButton.addActionListener(createClearAction(dto));
        return jButton;
    }

    private ActionListener createClearAction(TextFieldsDto dto) {
        ClearActionFactory factory = new ClearActionFactory();
        ActionListener actionListener = factory.create(dto);
        return actionListener;
    }

    private JButton createSaveButton(TextFieldsDto dto) {
        JButton jButton = new JButton("Save new Aeroplane");
        jButton.setName("Save new Aeroplane");
        jButton.setSize(createDimension());
        jButton.addActionListener(createSaveAction(dto));
        return jButton;
    }

    private ActionListener createSaveAction(TextFieldsDto dto) {
        SaveActionFactory factory = new SaveActionFactory();
        ActionListener actionListener = factory.create(dto);
        return actionListener;
    }

    private Dimension createDimension() {
        Dimension d = new Dimension();
        d.setSize(60, 20);
        return d;
    }
}
