package com.flight.gui.front.menu.file.newMenu.plane.insertButton;

import lombok.Data;

import javax.swing.*;
import java.util.ArrayList;
import java.util.List;

@Data
public class TextFieldsDto {

    JTextField baggage;
    JTextField basicEmptyWeight;
    JTextField fuel;
    JTextField maxCg;
    JTextField maxGrossWeight;
    JTextField minCg;
    JTextField model;
    JTextField name;
    JTextField passengerRearSeat;
    JTextField pilotAndFrontPassenger;
    JTextField moment;
    JTextField minUpperBoundCenterOfGravity;
    JTextField maxWeightOfLowerBound;
    JTextField maxCenterOfGravity;
    JTextField complexFunctionFxFactor;
    JTextField complexFunctionY;
    List<JTextField> list = new ArrayList<>();
}
