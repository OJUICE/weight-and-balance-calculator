package com.flight.gui.front.menu.file.exit;

import javax.swing.*;
import java.awt.event.ActionListener;

public class ExitFactory {

    public JMenuItem create() {
        JMenuItem item = new JMenuItem("Exit");
        item.addActionListener(createAction());
        return item;
    }

    private ActionListener createAction() {
        ActionFactory factory = new ActionFactory();
        ActionListener action = factory.create();
        return action;
    }
}
