package com.flight.gui.front.menu.file.newMenu;

import javax.swing.*;
import java.awt.event.ActionListener;

public class PlaneItemFactory {
    public JMenuItem create() {
        JMenuItem planeItem = new JMenuItem("Plane");
        planeItem.addActionListener(createAction());
        return planeItem;
    }

    private ActionListener createAction() {
        ActionFactory factory = new ActionFactory();
        ActionListener action = factory.create();
        return action;
    }
}
