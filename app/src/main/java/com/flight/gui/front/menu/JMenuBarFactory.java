package com.flight.gui.front.menu;

import com.flight.gui.front.menu.edit.EditFactory;
import com.flight.gui.front.menu.file.FileFactory;
import com.flight.gui.front.menu.view.ViewFactory;

import javax.swing.*;

public class JMenuBarFactory {

    public JMenuBar create() {
        JMenuBar bar = new JMenuBar();
        JMenu fileMenu = createFileMenu();
        JMenu editMenu = createEditMenu();
        JMenu viewMenu = createViewMenu();
        bar.add(fileMenu);
        bar.add(editMenu);
        bar.add(viewMenu);
        return bar;
    }

    private JMenu createFileMenu() {
        FileFactory factory = new FileFactory();
        JMenu jMenu = factory.create();
        return jMenu;

    }

    private JMenu createEditMenu() {
        EditFactory factory = new EditFactory();
        JMenu j = factory.create();
        return j;
    }

    private JMenu createViewMenu() {
        ViewFactory factory = new ViewFactory();
        JMenu jMenu = factory.create();
        return jMenu;
    }



}
