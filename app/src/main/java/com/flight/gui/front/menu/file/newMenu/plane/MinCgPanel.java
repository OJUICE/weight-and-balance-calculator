package com.flight.gui.front.menu.file.newMenu.plane;

import com.flight.gui.front.menu.file.newMenu.plane.insertButton.TextFieldsDto;

import javax.swing.*;
import java.awt.*;

public class MinCgPanel {
    public JPanel create(TextFieldsDto dto) {
        JPanel panel = new JPanel();
        panel.add(createLabel());
        JTextField mincg = createTextfield();
        dto.setMinCg(mincg);
        dto.getList().add(mincg);
        panel.add(mincg);
        panel.setLocation(10, 10 + 230);
        panel.setSize(365, 30);
        panel.setBackground(Color.LIGHT_GRAY);
        return panel;
    }

    private JLabel createLabel() {
        JLabel label = new JLabel("Min CG: ");
        return label;
    }

    private JTextField createTextfield() {
        JTextField text = new JTextField();
        text.setColumns(20);
        return text;
    }
}
