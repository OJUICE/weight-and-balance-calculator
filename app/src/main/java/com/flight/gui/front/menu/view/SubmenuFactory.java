package com.flight.gui.front.menu.view;

import javax.swing.*;

public class SubmenuFactory {

    public JMenu create() {
        JMenu subMenu = new JMenu("Other");
        JMenuItem subMenu1Item = createSubSubmenu1();
        JMenuItem subMenu2Item = createSubSubmenu2();
        subMenu.add(subMenu1Item);
        subMenu.add(subMenu2Item);
        return subMenu;
    }

    public JMenuItem createSubSubmenu1() {
        SubSubmenuFactory factory = new SubSubmenuFactory();
        JMenu jMenu = factory.create();
        return jMenu;
    }

    public JMenuItem createSubSubmenu2() {
        SubSubmenuFactory factory = new SubSubmenuFactory();
        JMenu jMenu = factory.create();
        return jMenu;
    }
}
