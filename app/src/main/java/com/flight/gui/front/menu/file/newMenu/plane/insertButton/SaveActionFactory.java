package com.flight.gui.front.menu.file.newMenu.plane.insertButton;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class SaveActionFactory {

    public ActionListener create(TextFieldsDto dto) {
        ActionListener ac = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("SQL Statement:\n");
                System.out.println(createSqlStatement(dto));
            }
        };
        return ac;
    }

    private String createSqlStatement(TextFieldsDto dto) {
        return "Insert into Plane values (\n" +
                createColumn("basic_empty_weight", dto.getBasicEmptyWeight()) + //3
                createColumn("basic_empty_moment", dto.getMoment()) + // 4
                createColumn("model", dto.getModel()) + //1
                createColumn("name", dto.getName()) + //2
                createColumn("pilot_and_front_passenger_arm", dto.getPilotAndFrontPassenger()) + // 5
                createColumn("passenger_rear_seat", dto.getPassengerRearSeat()) + //6
                createColumn("fuel", dto.getFuel()) + // 7
                createColumn("baggage", dto.getBaggage()) + // 8
                createColumn("min_center_of_gravity", dto.getMinCg()) + // 9
                createColumn("Min_Upper_Bound_Center_Of_Gravity", dto.getMinUpperBoundCenterOfGravity()) +
                createColumn("Max_Weight_Of_Lower_Bound", dto.getMaxWeightOfLowerBound()) +
                createColumn("Max_CG", dto.getMaxCg()) +
                createColumn("Max_Gross_Weight", dto.getMaxGrossWeight()) +
                createColumn("Complex_Function_Fx_Factor", dto.getComplexFunctionFxFactor()) +
                createColumn("Complex_Function_Y", dto.getComplexFunctionY()) +
                ");";
    }


    private String createColumn(String column, JTextField field) {
        if (null == field) {
            return column.toUpperCase() + " = '', \n";
        }
        return column.toUpperCase() + " = '" + field.getText() + "', \n";
    }
}
