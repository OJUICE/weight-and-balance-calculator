package com.flight.gui.name;

import com.flight.gui.Panels;
import com.flight.model.planes.Plane;
import lombok.Data;

import javax.swing.*;
import java.awt.*;

import static com.flight.gui.attributes.Constants.WIDTH;

@Data
public class PlaneModel {

    JLabel value;

    public JPanel create(Panels dto) {
        JPanel panel = new JPanel();
        panel.setSize(WIDTH, 50);
        panel.setMaximumSize(new Dimension(WIDTH, 200));
        panel.setLocation(10, 60);
        panel.setBackground(Color.RED);
        JLabel model = createModelLabel();
        Plane plane = dto.getPlaneName().getPlane();
        value = createValueLabel(plane);
        panel.add(model);
        panel.add(value);
        panel.setVisible(true);
        return panel;
    }


    private JLabel createModelLabel() {
        return new JLabel("Model:");
    }

    private JLabel createValueLabel(Plane plane) {
        return new JLabel(plane.getModel());
    }

    public JButton signalButton() {
        JButton signalButton = new JButton("Calculate Weight and Balance");
        return signalButton;
    }
}
