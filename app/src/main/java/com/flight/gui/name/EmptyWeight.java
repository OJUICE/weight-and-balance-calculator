package com.flight.gui.name;

import com.flight.gui.Panels;
import com.flight.model.planes.Plane;
import lombok.Data;

import javax.swing.*;
import java.awt.*;

import static com.flight.gui.attributes.Constants.WIDTH;

@Data
public class EmptyWeight {

    JLabel emptyWeightLabel;
    JLabel minCgLabel;
    JLabel maxCgLabel;

    public JPanel create(Panels dto) {
        JPanel panel = new JPanel();
        panel.setSize(WIDTH, 50);
        panel.setMaximumSize(new Dimension(WIDTH, 200));
        panel.setLocation(10, 60 + 50);
        panel.setBackground(Color.RED);
        JLabel model = createWeightLabel();
        Plane plane = dto.getPlaneName().getPlane();
        emptyWeightLabel = createValueLabel(plane);
        minCgLabel = createMinCg(plane);
        JLabel space1 = createSpace();
        JLabel space2 = createSpace();
        maxCgLabel = createMaxCg(plane);
        panel.add(model);
        panel.add(emptyWeightLabel);
        panel.add(space1);
        panel.add(minCgLabel);
        panel.add(space2);
        panel.add(maxCgLabel);
        panel.setVisible(true);
        return panel;
    }

    private JLabel createSpace() {
        return new JLabel("    ");
    }

    private JLabel createMaxCg(Plane plane) {
        return new JLabel("Max CG: " + plane.getMaxCenterOfGravity());
    }

    private JLabel createMinCg(Plane plane) {
        return new JLabel("Min CG: " + plane.getMinCenterOfGravity());
    }

    private JLabel createWeightLabel() {
        return new JLabel("Empty Weight:");
    }

    private JLabel createValueLabel(Plane plane) {
        return new JLabel("" + plane.getBasicEmptyWeight().getWeight() + "lbs");
    }
}
