package com.flight.gui.name;

import com.flight.model.planes.Plane;
import com.flight.repository.PlaneRepository;
import lombok.Data;

import javax.swing.*;
import java.awt.*;

import static com.flight.gui.attributes.Constants.WIDTH;

@Data
public class PlaneName {

    PlaneRepository planeRepository = new PlaneRepository();
    JComboBox<String> cb;

    public JPanel createDropDown() {
        JPanel panel = new JPanel();
        panel.setSize(WIDTH, 50);
        panel.setLocation(10, 10);
        panel.setBackground(Color.GREEN);
        JLabel lbl = new JLabel("Select one of the possible choices and click OK");
        lbl.setVisible(true);
        panel.add(lbl);
        String[] planes = planeRepository.getPlanes().stream().map(Plane::getName).toArray(String[]::new);
        cb = new JComboBox<>(planes);
        cb.setVisible(true);
        panel.add(cb);
        return panel;
    }

    public Plane getPlane() {
        return planeRepository.findByName(getPlaneNameFromDropdown());
    }

    private String getPlaneNameFromDropdown() {
        return String.valueOf(cb.getSelectedItem());
    }
}
