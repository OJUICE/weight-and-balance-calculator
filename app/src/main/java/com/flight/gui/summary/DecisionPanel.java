package com.flight.gui.summary;

import lombok.Data;

import javax.swing.*;
import java.awt.*;

import static com.flight.gui.attributes.Constants.WIDTH;

@Data
public class DecisionPanel {

    private JTextField isTotalWeightOk;
    private JTextField isCgOk;

    public JPanel create() {
        JPanel panel = new JPanel();
        panel.setSize(WIDTH, 50);
        panel.setMaximumSize(new Dimension(WIDTH, 200));
        panel.setLocation(10, 60 + 50 + 50 + 50 + 50 + 50 + 100 + 50);
        panel.setBackground(Color.RED);
        panel.add(createTotalWeightLabel());
        isTotalWeightOk = createTotalWeightTextField();
        panel.add(isTotalWeightOk);
        panel.add(createCenterOfGravityDescription());
        isCgOk = createCenterOfGravityTextField();
        panel.add(isCgOk);
        panel.setVisible(true);
        return panel;
    }

    private JLabel createTotalWeightLabel() {
        return new JLabel("Is Total Weight OK:");
    }

    private JTextField createTotalWeightTextField() {
        JTextField jTextField = new JTextField();
        jTextField.setColumns(5);
        jTextField.setEditable(false);
        return jTextField;
    }

    private JLabel createCenterOfGravityDescription() {
        return new JLabel("Is the plane balanced:");
    }

    private JTextField createCenterOfGravityTextField() {
        JTextField jTextField = new JTextField();
        jTextField.setColumns(5);
        jTextField.setEditable(false);
        return jTextField;
    }
}
