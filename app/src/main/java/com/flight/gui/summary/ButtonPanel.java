package com.flight.gui.summary;

import com.flight.gui.Panels;
import com.flight.gui.summary.button.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

import static com.flight.gui.attributes.Constants.WIDTH;

public class ButtonPanel {


    public JPanel create(Panels input) {
        JPanel panel = new JPanel();
        panel.setSize(WIDTH, 50);
        panel.setMaximumSize(new Dimension(WIDTH, 200));
        panel.setLocation(10, 60 + 50 + 50 + 50 + 50 + 50 + 50);
        panel.setBackground(Color.RED);
        JButton calcButton = createButton(input);
        panel.add(calcButton);
        panel.setVisible(true);
        return panel;
    }

    private JButton createButton(Panels input) {
        JButton button = new JButton();
        button.setText("Calculate Center of Gravity");
        button.setVisible(true);
        button.addActionListener(createButtonAction(input));
        return button;
    }

    private ActionListener createButtonAction(Panels dto) {
        System.out.println("Button clicked");
        return e -> {
            setTotalWeightLabelInTotalWeightPanel(dto);
            setCenterOfGravityLabelInTotalWeightPanel(dto);
            setIsTotalWeightOkInDecisionPanel(dto);
            setIsCgOkInDecisionPanel(dto);
            setPlaneModelPanel(dto);
            setEmptyWeightPanel(dto);
            setCgMinLabel(dto);
            setCgMaxPanel(dto);
        };
    }


    private void setTotalWeightLabelInTotalWeightPanel(Panels dto) {
        TotalWeightService totalWeightService = new TotalWeightService();
        totalWeightService.setTotalWeightLabelInTotalWeightPanel(dto);
    }

    private void setCenterOfGravityLabelInTotalWeightPanel(Panels dto) {
        CenterOfGravityService centerOfGravityService = new CenterOfGravityService();
        centerOfGravityService.setCenterOfGravityLabelInTotalWeightPanel(dto);
    }

    private void setIsTotalWeightOkInDecisionPanel(Panels dto) {
        TotalWeightOkService totalWeightOkService = new TotalWeightOkService();
        totalWeightOkService.setIsTotalWeightOkInDecisionPanel(dto);
    }

    private void setIsCgOkInDecisionPanel(Panels dto) {
        CgOkService cgOkService = new CgOkService();
        cgOkService.setIsCgOkInDecisionPanel(dto);
    }

    private void setPlaneModelPanel(Panels dto) {
        PlaneModelService planeModelService = new PlaneModelService();
        planeModelService.setPlaneModelPanel(dto);
    }

    private void setEmptyWeightPanel(Panels dto) {
        EmptyWeightService emptyWeightService = new EmptyWeightService();
        emptyWeightService.setEmptyWeightPanel(dto);
    }

    private void setCgMinLabel(Panels dto) {
        CgMinService cgMinService = new CgMinService();
        cgMinService.setCgMinLabel(dto);
    }

    private void setCgMaxPanel(Panels dto) {
        CgMaxService cgMaxService = new CgMaxService();
        cgMaxService.setCgMaxLabel(dto);
    }

}
