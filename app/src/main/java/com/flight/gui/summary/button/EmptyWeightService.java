package com.flight.gui.summary.button;

import com.flight.gui.Panels;
import com.flight.gui.name.EmptyWeight;
import com.flight.gui.name.PlaneName;
import com.flight.model.planes.Plane;
import com.flight.model.planes.WeightAndArm;

import javax.swing.*;

public class EmptyWeightService {

    public void setEmptyWeightPanel(Panels dto) {
        //get panel
        EmptyWeight emptyWeightPanel = dto.getEmptyWeight();

        //where (destination) get label
        JLabel emptyWeightLabel = emptyWeightPanel.getEmptyWeightLabel();

        //what (content) get String
        String whatToSet = getEmptyWeight(dto);

        //UPDATE emptyWegithLabel set content = whatToSet where emptyWeightPanel.getEmptyWeight()
        emptyWeightLabel.setText(whatToSet);
    }

    private String getEmptyWeight(Panels dto) {
        PlaneName panel = dto.getPlaneName();
        Plane plane = panel.getPlane();
        WeightAndArm w = plane.getBasicEmptyWeight();
        double weight = w.getWeight();
        return Double.toString(weight);
    }
}
