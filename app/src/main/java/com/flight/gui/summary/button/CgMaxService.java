package com.flight.gui.summary.button;

import com.flight.gui.Panels;
import com.flight.gui.name.EmptyWeight;
import com.flight.gui.name.PlaneName;
import com.flight.model.planes.Plane;

import javax.swing.*;

public class CgMaxService {

    public void setCgMaxLabel(Panels dto) {
        EmptyWeight emptyWeightPanel = dto.getEmptyWeight();
        JLabel maxCgLabel = emptyWeightPanel.getMaxCgLabel();
        String maxCg = getMaxCgFromPlane(dto);
        maxCgLabel.setText(maxCg);
    }

    private String getMaxCgFromPlane(Panels dto) {
        PlaneName planeNamePanel = dto.getPlaneName();
        Plane p1 = planeNamePanel.getPlane();
        return "Max CG: " + p1.getMaxCenterOfGravity();
    }
}
