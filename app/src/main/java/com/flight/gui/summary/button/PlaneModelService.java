package com.flight.gui.summary.button;

import com.flight.gui.Panels;
import com.flight.gui.name.PlaneModel;
import com.flight.gui.name.PlaneName;
import com.flight.model.planes.Plane;

import javax.swing.*;

public class PlaneModelService {

    public void setPlaneModelPanel(Panels dto) {
        PlaneModel panel = dto.getPlaneModel();
        JLabel label = panel.getValue();
        String text = getModel(dto);
        label.setText(text);
    }


    private String getModel(Panels dto) {
        PlaneName panel = dto.getPlaneName();
        Plane plane = panel.getPlane();
        String model = plane.getModel();
        return model;
    }
}
