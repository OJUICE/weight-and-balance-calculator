package com.flight.gui.summary.button;

import com.flight.gui.Panels;
import com.flight.gui.summary.DecisionPanel;
import com.flight.model.planes.RequiredParameters;
import com.flight.service.LimitService;

import javax.swing.*;

public class CgOkService {

    LimitService ls = new LimitService();

    public void setIsCgOkInDecisionPanel(Panels dto) {
        DecisionPanel d = dto.getDecisionPanel();
        JTextField isCgOk = d.getIsCgOk();
        String cgOk = isCgOk(dto);
        isCgOk.setText(cgOk);
    }

    private String isCgOk(Panels dto) {
        boolean balanced = ls.isBalanced(createInput(dto));
        return Boolean.toString(balanced);
    }

    private RequiredParameters createInput(Panels dto) {
        return RequiredParameters.builder()
                .pilotAndFrontPassengerWeight(dto.getPilotAndFrontPassengerWeight().getValue())
                .passengerRearSeatWeight(dto.getPassengerRearSeatWeight().getValue())
                .baggageWeight(dto.getBaggageWeight().getValue())
                .fuelWeight(dto.getFuelWeight().getValue())
                .plane(dto.getPlaneName().getPlane().getName())
                .build();
    }
}
