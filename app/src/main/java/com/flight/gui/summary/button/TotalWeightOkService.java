package com.flight.gui.summary.button;

import com.flight.gui.Panels;
import com.flight.gui.summary.DecisionPanel;
import com.flight.model.planes.RequiredParameters;
import com.flight.service.LimitService;

import javax.swing.*;

public class TotalWeightOkService {

    LimitService ls = new LimitService();

    public void setIsTotalWeightOkInDecisionPanel(Panels dto) {
        DecisionPanel panel = dto.getDecisionPanel();
        JTextField text = panel.getIsTotalWeightOk();
        String totalWeightOk = isTotalWeightOk(dto);
        text.setText(totalWeightOk);
    }

    private String isTotalWeightOk(Panels dto) {
        RequiredParameters input = createInput(dto);
        boolean balanced = ls.isTotalWeightInRange(input);
        return Boolean.toString(balanced);
    }

    private RequiredParameters createInput(Panels dto) {
        return RequiredParameters.builder()
                .pilotAndFrontPassengerWeight(dto.getPilotAndFrontPassengerWeight().getValue())
                .passengerRearSeatWeight(dto.getPassengerRearSeatWeight().getValue())
                .baggageWeight(dto.getBaggageWeight().getValue())
                .fuelWeight(dto.getFuelWeight().getValue())
                .plane(dto.getPlaneName().getPlane().getName())
                .build();
    }
}
