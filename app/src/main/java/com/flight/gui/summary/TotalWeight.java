package com.flight.gui.summary;

import lombok.Data;

import javax.swing.*;
import java.awt.*;

import static com.flight.gui.attributes.Constants.WIDTH;

@Data
public class TotalWeight {

    private JTextField totalWeight;
    private JTextField centerOfGravity;

    public JPanel create() {
        JPanel panel = new JPanel();
        panel.setSize(WIDTH, 50);
        panel.setMaximumSize(new Dimension(WIDTH, 200));
        panel.setLocation(10, 60 + 50 + 50 + 50 + 50 + 50 + 100);
        panel.setBackground(Color.RED);
        panel.add(createTotalWeightLabel());
        totalWeight = createTotalWeightTextField();
        panel.add(totalWeight);
        panel.add(createCenterOfGravityDescription());
        centerOfGravity = createCenterOfGravityTextField();
        panel.add(centerOfGravity);
        panel.setVisible(true);
        return panel;
    }

    private JLabel createTotalWeightLabel() {
        return new JLabel("Total Weight:");
    }

    private JTextField createTotalWeightTextField() {
        JTextField jTextField = new JTextField();
        jTextField.setColumns(10);
        jTextField.setEditable(false);
        return jTextField;
    }

    private JLabel createCenterOfGravityDescription() {
        return new JLabel("CG:");
    }

    private JTextField createCenterOfGravityTextField() {
        JTextField jTextField = new JTextField();
        jTextField.setColumns(10);
        jTextField.setEditable(false);
        return jTextField;
    }
}
