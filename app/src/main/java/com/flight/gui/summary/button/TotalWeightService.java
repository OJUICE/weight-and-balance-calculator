package com.flight.gui.summary.button;

import com.flight.gui.Panels;
import com.flight.gui.name.PlaneName;
import com.flight.gui.summary.TotalWeight;
import com.flight.model.planes.Plane;
import com.flight.model.planes.RequiredParameters;

import javax.swing.*;
import java.math.BigDecimal;
import java.math.RoundingMode;

public class TotalWeightService {

    public void setTotalWeightLabelInTotalWeightPanel(Panels dto) {
        TotalWeight panel = dto.getTotalWeight();
        JTextField text = panel.getTotalWeight();
        String value = getTotalWeightRounded(dto);
        text.setText(value);
    }

    private String getTotalWeightRounded(Panels dto) {
        double d = calculateTotalWeight(dto);
        BigDecimal rounded = BigDecimal.valueOf(d).setScale(2, RoundingMode.HALF_UP);
        return rounded.toString();
    }

    private double calculateTotalWeight(Panels dto) {
        PlaneName panel = dto.getPlaneName();
        Plane p = panel.getPlane();
        RequiredParameters input = createInput(dto);
        return p.calculateTotalWeight(input);
    }

    private RequiredParameters createInput(Panels dto) {
        return RequiredParameters.builder()
                .pilotAndFrontPassengerWeight(dto.getPilotAndFrontPassengerWeight().getValue())
                .passengerRearSeatWeight(dto.getPassengerRearSeatWeight().getValue())
                .baggageWeight(dto.getBaggageWeight().getValue())
                .fuelWeight(dto.getFuelWeight().getValue())
                .plane(dto.getPlaneName().getPlane().getName())
                .build();
    }
}
