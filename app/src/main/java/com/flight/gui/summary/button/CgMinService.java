package com.flight.gui.summary.button;

import com.flight.gui.Panels;
import com.flight.gui.name.EmptyWeight;
import com.flight.model.planes.Plane;

import javax.swing.*;

public class CgMinService {

    /**
     * This method finds the second label from the second panel in the main Frame.
     * and it updates the value of this label with the calc value.
     *
     * @param dto
     */
    public void setCgMinLabel(Panels dto) {
        // 1. find the right panel
        EmptyWeight emptyWeightPanel = dto.getEmptyWeight();

        // 2. find the right label in the panel from 1
        JLabel minCgLabel = emptyWeightPanel.getMinCgLabel();

        // 3. find the content that you want to display on the screen (what value)
        String minCg = getMinCgFromPlane(dto);

        // 4. execute update (where.setText(what))
        minCgLabel.setText(minCg);
    }

    private String getMinCgFromPlane(Panels dto) {
        Plane p = dto.getPlaneName().getPlane();
        return "Min CG: " + p.getMinCenterOfGravity();
    }
}
