package com.flight.gui;

import com.flight.gui.attributes.BaggageWeight;
import com.flight.gui.attributes.FuelWeight;
import com.flight.gui.attributes.PassengerRearSeatWeight;
import com.flight.gui.attributes.PilotAndFrontPassengerWeight;
import com.flight.gui.front.menu.JMenuBarFactory;
import com.flight.gui.name.EmptyWeight;
import com.flight.gui.name.PlaneModel;
import com.flight.gui.name.PlaneName;
import com.flight.gui.summary.ButtonPanel;
import com.flight.gui.summary.DecisionPanel;
import com.flight.gui.summary.TotalWeight;
import lombok.AccessLevel;
import lombok.experimental.FieldDefaults;

import javax.swing.*;
import java.awt.*;

@FieldDefaults(level = AccessLevel.PRIVATE)
public class MainFrame {

    PlaneName planeName = new PlaneName();
    PlaneModel planeModel = new PlaneModel();
    EmptyWeight emptyWeight = new EmptyWeight();
    PilotAndFrontPassengerWeight pilotW = new PilotAndFrontPassengerWeight();
    PassengerRearSeatWeight rearW = new PassengerRearSeatWeight();
    FuelWeight fuelWeight = new FuelWeight();
    BaggageWeight baggageWeight = new BaggageWeight();
    ButtonPanel buttonPanel = new ButtonPanel();
    TotalWeight totalWeight = new TotalWeight();
    DecisionPanel decisionPanel = new DecisionPanel();

    public JFrame createMainWindow() {
        JFrame frame = new JFrame("Weight and Balance Calculator");
        frame.add(createBackground());
        frame.setSize(800, 600);
        frame.setLocationRelativeTo(null);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setJMenuBar(createMenuBar());
        frame.setVisible(true);
        return frame;
    }

    private JMenuBar createMenuBar() {
        JMenuBarFactory factory = new JMenuBarFactory();
        return factory.create();
    }

    public JPanel createBackground() {
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        jPanel.setSize(800, 600);
        jPanel.setBackground(Color.BLUE);
        jPanel.add(planeName.createDropDown());
        Panels dto = createDto();
        jPanel.add(planeModel.create(dto));
        jPanel.add(emptyWeight.create(dto));
        jPanel.add(pilotW.create());
        jPanel.add(rearW.create());
        jPanel.add(fuelWeight.create());
        jPanel.add(baggageWeight.create());
        jPanel.add(buttonPanel.create(dto));
        jPanel.add(totalWeight.create());
        jPanel.add(decisionPanel.create());
        return jPanel;
    }

    private Panels createDto() {
        return Panels.builder()
                .planeName(planeName)
                .planeModel(planeModel)
                .emptyWeight(emptyWeight)
                .pilotAndFrontPassengerWeight(pilotW)
                .passengerRearSeatWeight(rearW)
                .fuelWeight(fuelWeight)
                .baggageWeight(baggageWeight)
                .totalWeight(totalWeight)
                .decisionPanel(decisionPanel)
                .build();
    }
}
