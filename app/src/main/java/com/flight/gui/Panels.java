package com.flight.gui;

import com.flight.gui.attributes.BaggageWeight;
import com.flight.gui.attributes.FuelWeight;
import com.flight.gui.attributes.PassengerRearSeatWeight;
import com.flight.gui.attributes.PilotAndFrontPassengerWeight;
import com.flight.gui.name.EmptyWeight;
import com.flight.gui.name.PlaneModel;
import com.flight.gui.name.PlaneName;
import com.flight.gui.summary.DecisionPanel;
import com.flight.gui.summary.TotalWeight;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class Panels {

    PlaneName planeName;
    EmptyWeight emptyWeight;
    PlaneModel planeModel;
    PilotAndFrontPassengerWeight pilotAndFrontPassengerWeight;
    PassengerRearSeatWeight passengerRearSeatWeight;
    FuelWeight fuelWeight;
    BaggageWeight baggageWeight;
    TotalWeight totalWeight;
    DecisionPanel decisionPanel;
}
