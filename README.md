# Weight And Balance Calculator

This calculator allows your to calculate the center of gravity envelope for multiple aircraft

# Project Plan

1) Configure Environment
- set up git repository at gitlab.com
- create local repository and connect with remote repository
- install new JDK (Java Development Kit)
- install new gradle
- create an empty gradle project in IntelliJ
- git add/commit/push
2) Model Data and Constants/Enums
- crate classes
- add values/constants
3) create 3-Tier Architecture
- add controller
- add service
- add repository
4) write Tests (Test-Driven-Development)
- import AssertJ library
- create tests for services
- create tests for data objects
5) prepare GUI
- create prototype
- implement functionality


2021-02-21
alias java='/c/Program\ Files/Java/jdk-15.0.2/bin/java.exe'
alias gradle='/c/Program\ Files/Gradle/gradle-6.8.2/bin/gradle'


f(83) = 1950
f(87) = 2440 


a 83 + b = 1950
a 87 + b = 2440

b = 1950 - a 83
a 87 + b = 2440

b = 1950 - a 83
a 87 + (1950 - a 83) = 2440
a87 + 1950 -a83 = 2440 | -1950
a4 = 490 | :4
a = 122.5

b = 1950 - a  83
b = 1950 - 122.5 * 83 | 
b = 1950 - 10167.5
b =  -8217,5

f(x) = 122.5x - 8217.5
f(86) = 122.5 * 86 - 8217.5 = 2317.5
f(87) = 122.5 * 87 - 8217.5 = 2440
f(83) = 122.5 * 83 - 8217.5 = 1950




f(x) = ax + b

point (x = 1, y = 2) false
point (x = 1, y = 1) true
point (x = 2, y = 1) true

step 1:
calculcate what is f(x) of the function
f(x) = 1

step 2:
is 1 >= point y

Test1: x 82 y 2400 assertFalse
Test2: x 84 y 2400 assert False
Test3: x 86 y 2400 assertFalse

Test4: x 82 y 2200 assertFalse3
Test5: x 84 y 2200 assert False
Test6: x 86 y 2200 assertTrue

Test7: x 82 y 2000 assertFalse3
Test8: x 84 y 2000 assertTrue
Test9: x 86 y 2000 assertTrue
Test10:x 82 y 1800 asssertFalse


Write tests for UpperBound
10 tests

Write tests for LowerBound
10 tests

Write tests for all (isBalanced)
30 test


3/14/2021:
Calculate Center of Gravity:
1. CG JTextflield must be accessible from outside (getter and instance var) done.
-- 2. Total weight Field Panel must be accessible inside button panel
3. add Total Weight Panel to list of Parameters inside of Dto (RequiredParametersDto) l.15  done.
4. set Total Weight panel inside of Dto  l.61 done.
5. Read total weight panel from Dto  l.38 in ButtonPanel
6. Read CG Textfield from total weight panel from 5.
7. set Text inside of CG text field
8. calculate centerOfGravity. It would be best if you create a method for this in the same class.

 
Next Lesson:
Select plane dynamically:
1. Plane dropdown must be accessible from outside of PlaneName panel(getter and instance var) done
-- 2. PlaneName Panel must be accessible inside button panel    done.
3. add PlaneName Panel to list of Parameters inside of Dto (RequiredParametersDto) l.16.  done 
4. set PlaneName panel inside of Dto  l.62.
5. Read PlaneName panel from Dto  l.39 in ButtonPanel
6. Read dropdown from PlaneName panel from 5.
7. pass real PlaneName instead of fake plane to the method (as a parameter)
8. calculate total weight and centerOfGravity with real plane.


3/20/2021:
Button has to update values in top panel
create 4 methods: Model done, minCG, max CG, Empty Weight done
	1. Create Method with a nice name
	2. Localize and access Panel/Label that is going to be updated
	3. set Text of JLabel that you found in point 2
	4. get Content of the Text that you use in point nr 3 (what to set as Text in there)
group all 8 methods calls into 4 or less descriptive methods.


## IntelliJ Shortcuts

Search Everywhere --> Shift Shift
Search in Path --> Ctrl + Shift + F
extract method --> Ctrl + Alt + M
extract variable --> Ctrl + Alt + V
extract field --> Ctrl + Alt + F
fix compilation error --> Alt + Enter
display more options --> Ctrl + Space
